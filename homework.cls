%% homework.cls
%% Copyright 2021 Severino Trotta
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Severino Trotta.
%
% This work consists of the following files:
% template.tex, homework.cls

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{homework}[2021/04/04 Homework template class]





%% ------------------- PACKAGES ------------------- %%
%\RequirePackage{scrlfile}						% provides commands for package loading order.
\RequirePackage{calc}							% do calculations with numeric variables.
\RequirePackage{xparse}							% NewDocumentCommand without XeLaTeX/LuaLaTeX.
\RequirePackage[utf8]{inputenc}					% UTF-8 charset when compiling with pdfLaTeX. Obsolete in XeLaTeX.
\RequirePackage[default, scale=0.82]{opensans}	% sets font.
\RequirePackage[defaultmono, scale=0.82]{droidmono}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{tcolorbox}						% used for environments and page banner.
\RequirePackage[style=iso, calc]{datetime2}		% needed for \today format.



%% ------------------- COLORS ------------------- %%
\newcommand{\tintlet}[1]{%
	% Sets tint color with provided color names/mix.
	\colorlet{global}{#1}
	\colorlet{global_light}{global!80!white}	\colorlet{global_dark}{global!90!black}	\colorlet{global_text}{global!30!black}	\colorlet{global_theorem_text}{global!20!black}	\colorlet{global_bg}{global!20}
	\colorlet{global_banner_bg}{global_dark}	\colorlet{global_banner_text}{white}	\colorlet{global_header}{global_dark!95!black}
}
\newcommand{\tintset}[2]{
	% Sets tint color with provided color definition.
	\definecolor{tint_tmp}{#1}{#2}
	\tintlet{tint_tmp}
	\definecolor{global}{#1}{#2}
}
\tintset{RGB}{80,80,120}





%% ------------------- TEXT & FORMATTING ------------------- %%
% Global text parameters:
\newcommand*{\HWtext@contributors}{}
\newcommand*{\HWtext@group}{}
\newcommand*{\HWtext@groupdividerchar}{}
\newcommand*{\HWtext@subtitledividerchar}{}
\newcommand*{\HWtext@course}{}
\newcommand*{\HWtext@semesterPart}{}	

% Commands to edit text parameters:
\newcommand{\subtitle}[1]{\renewcommand{\HWtext@course}{#1}}
\newcommand{\semester}[1]{\renewcommand{\HWtext@semesterPart}{#1}}
\newcommand{\contributors}[1]{%
	\renewcommand{\HWtext@contributors}{#1}
%	\author{#1}	% TODO: use this instead? Link both?
}
\newcommand{\tutorialgroup}[1]{\renewcommand{\HWtext@group}{#1}}
\newcommand{\tutorialdivider}[1]{\renewcommand{\HWtext@groupdividerchar}{#1}}
\newcommand{\subtitledivider}[1]{\renewcommand{\HWtext@subtitledividerchar}{#1}}

\renewcommand*{\@title}{}	% set default title to prevent error in article class.
% TODO: what about author?

% Counters:
\newcounter{series}
\setcounter{series}{0}

% Formatting:
\renewcommand*{\thefootnote}{[\arabic{footnote}]}	% format footnotes.
\DTMsetdatestyle{iso}











%% Modules
% TODO: add



%% OPTIONS
% TODO: use kvoptions.
% - basic: enumeration/boxes, math, graphics
% - graphics: floats, captions
% - plots: add plotting packages (pgfplots, tikz)
% - code: add source code environments (tcolorbox, minted, fonts). Requires (Xe|Lua)LaTeX.
% - ...
% - extended: ?
% - full: all other options
\newif\if@loadbasic %\@loadbasicfalse
\newif\if@loadmathdefs 
\newif\if@loadgraphics 
\newif\if@loaddrawing
\newif\if@loadtables
\newif\if@loadcode
%\newif\if@loadfonts
\newif\if@loadfull

\newif\if@nobanner

% Class options (functional)
% TODO: increase modularity.
\DeclareOption{basic}{\@loadbasictrue}
\DeclareOption{mathdefs}{\@loadmathdefstrue}
\DeclareOption{graphics}{\@loadgraphicstrue}
\DeclareOption{drawing}{\@loaddrawingtrue}
\DeclareOption{tables}{\@loadtablestrue}
%\DeclareOption{fonts}{\@loadfontstrue}	% TODO.
%\DeclareOption{code}{\@loadcodetrue}	% TODO.
\DeclareOption{extended}{
	\ExecuteOptions{basic, mathdefs, graphics, tables}
}
\DeclareOption{full}{
	\ExecuteOptions{basic, mathdefs, graphics, tables, drawing
%	, code
	}
}


% Class options (content)
% TODO: migrate all options to kvoptions.
\DeclareOption{nobanner}{\@nobannertrue}
%\RequirePackage{kvoptions}
%\DeclareBoolOption{nobanner}


% Remaining option setup:
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ExecuteOptions{basic}	% TODO: keep or remove?
\ProcessOptions*\relax
\LoadClass[a4paper, 11pt]{article}



%%%% Execute class options.
\if@loadbasic
	\RequirePackage{graphicx, amsmath, amssymb}
	\RequirePackage[inline]{enumitem}				% enumeration.
	\RequirePackage[
		colorlinks=true, 
		urlcolor=global!60, 
		linkcolor=global!60	
	]{hyperref}
	\RequirePackage{cleveref}
	\RequirePackage{relsize}
	
	% --- Environments and commands:
	\renewcommand{\thempfootnote}{[\arabic{mpfootnote}]}		% TCB footnote label format.

	% Variables for problem env:
	\newcounter{problem}
	\newcounter{lastproblem}
	% Formatters:
	\providecommand*{\ProblemNumberFormat}{\noexpand\theseries.\noexpand\arabic{problem}}
	\providecommand*{\ProblemTitlePrefix}{Problem}
	\providecommand*{\SubtaskPtsWidth}{\widthof{\quad\textbf{(10P)}}}
	% Setter for formatters:
	\providecommand*{\SetProblemNumberFormat}[1]{\renewcommand{\ProblemNumberFormat}{#1}}
	\providecommand*{\SetProblemTitlePrefix}[1]{\renewcommand{\ProblemTitlePrefix}{#1}}
	\providecommand*{\SetSubtaskPtsWidth}[1]{\renewcommand{\SubtaskPtsWidth}{#1}}
	
	% Problem env.
	\tcbuselibrary{many, skins}
	\newtcbtheorem[use counter=problem, number freestyle=\ProblemNumberFormat]{task}{\ProblemTitlePrefix}%
	{	skin=enhanced, breakable,
		%theorem style=plain,	%activate for theorem use.
	  	colback=global_bg!30, 
	  	colframe=global!30, 
	  	boxrule=1pt,
	  	% 
	  	fonttitle=\bfseries,
		coltitle=global_dark,
	   	attach boxed title to top left={
	   		xshift=5pt, 
	   		yshift=-27pt, 
	   		yshifttext=-8mm
	   	},
	   	boxed title style={
	   		colframe=global_bg!30, 
	   		colback=global_bg!30, arc=4pt,
	   	},
		coltext=global_theorem_text,
	   	before skip= %1cm
	   	{
	   		\ifnum\the\value{problem}=1		% use different skip length for the first instance of this theorem.
	  		0.5cm
			\else 
			0.1\linewidth
			\fi
		},
	   	%after skip=1cm,
	   	enlargepage flexible=5mm,
	   	pad after break=2mm,
%	   	code = {\stepcounter{lastproblem}}
	}{th}
	
	% Custom list for tasks of a problem.
	\newcommand*{\tllabel}{\color{global}\textbf{\arabic*)}}
	\newlist{tl}{enumerate}{1}
	\setenumerate[tl]{label=\tllabel, leftmargin=0.7cm, itemsep=0pt}
	\providecommand{\SetSubtaskLabelFormat}[1]{\renewcommand*{\tllabel}{#1}}
	
	% Custom environment for tasks, which creates a list bullet point.
	% The list is resumed inside each problem environment.
	% If no optional grade points are given, the full text width is used.
	\DeclareDocumentEnvironment{subtask}{ o }{%
		\ifnum\value{lastproblem}=\value{problem}
			\begin{tl}[resume=tasklist]
		\else	
			\setcounter{lastproblem}{\value{problem}}
			\begin{tl}[series=tasklist]
		\fi
		\item
		\IfNoValueTF{#1}{%
			\begin{minipage}[t]{1.0\linewidth}%
		}{%
			\begin{minipage}[t]{1.0\linewidth-\SubtaskPtsWidth}%	
		}
	}{%
		\end{minipage}%
		\label{task:\the\value{problem}.\the\value{tli}}%	% automatic labeling.
		\IfNoValueF{#1}{\hfill\pts{#1}}
		\end{tl}
	}

	
	% Call current font size.
	\providecommand{\currentfontsize}{\fontsize{\f@size}{\f@baselineskip}\selectfont}
	
	% Add grading points to problem.
	% TODO: Very hacky. Replace with more robust version (e.g. integrate into task/task list  )
	\NewDocumentCommand{\allpts}{m O{global!60}}{\vskip-4ex\hfill \textbf{\color{#2}(#1)}\vskip1.5ex}	% problem
	\newcommand{\pts}[2][global!60]{\mbox{}\hfill \textbf{\color{#1}(#2)}}	% subtask
\fi


%%% MATHDEFS OPTION DEFINITION
\if@loadmathdefs
	\RequirePackage{
		amsmath,
		amssymb,								% symbols
		marvosym,								% for lightning bolts in math. 		wasysym,								% for lightning bolts in math.
	}
	\RequirePackage[mathcal]{eucal}
	\RequirePackage{bbm}							% double-stroke for math symbols.
	\RequirePackage{nicefrac, centernot, mathtools}	\mathtoolsset{centercolon}
	
	% Sets
	\providecommand*{\N}{\mathbb{N}}
	\newcommand*{\Z}{\mathbb{Z}}
	\newcommand*{\Q}{\mathbb{Q}}
	\newcommand*{\R}{\mathbb{R}}
	\newcommand*{\Complex}{\mathbb{C}}
	\DeclareMathOperator{\setcomp}{\ensuremath{{^\complement}}}
	
	%% Symbols % Operators
	\newcommand{\cupdot}{\mathbin{\mathaccent\cdot\cup}}		% disjoint union.
	\newcommand*{\id}{\operatorname{id}}						% identity function.
	\newcommand*{\iu}{\mathrm{i}}							% imaginary unit.
	\newcommand{\norm}[1]{{\left\lVert #1 \right\rVert}}
	\newcommand*{\del}{\partial}								% shortcut for partial derivative.
	\DeclareMathOperator{\Lagr}{\mathcal{L}}					% Lagrange operator.
	\newcommand{\rt}[1][]{\sqrt[\leftroot{3}#1]}				% n-th root.
	\newcommand*{\T}{{\mkern-1.5mu\mathsf{T}}}				% matrix transpose symbol.
	\newcommand*{\CT}{{\mkern-1.5mu\mathsf{H}}}				% matrix conjugate transpose symbol.
	\newcommand*{\inv}{^{-1}}
	\newcommand*{\ceil}[1]{\ensuremath{\lceil #1 \rceil}}
	\newcommand*{\floor}[1]{\ensuremath{\lfloor #1 \rfloor}}
	\newcommand*{\abs}[1]{\ensuremath{\left| #1 \right|}}
	\newcommand*{\set}[1]{\ensuremath{\left\{#1\right\}}}
	\newcommand{\opn}[1]{\operatorname{#1}}
	\newcommand{\placeh}[1][0.6]{\,\vcenter{\hbox{\scalebox{#1}{$\bullet$}}}\,}
	\newcommand*{\sgn}{\operatorname{sgn}}
	\newcommand{\QED}[1][global]{\tag*{\bsq[#1]}}			% QED symbol to use inside math.
	\newcommand{\qed}[1][global]{\hfill\bsq[#1]}				% \QED for text-mode.
	\newcommand{\NQED}[1][global]{\tag*{\bcont[#1]}}			% contradiction symbol.
	\newcommand{\nqed}[1][global]{\hfill\bcont[#1]}			% \NQED for text-mode.
	\newcommand*{\nfrac}{\nicefrac}
	\newcommand{\eunorm}[1]{\ensuremath{|\hskip-0.2ex|#1|\hskip-0.2ex|}}		% shortcut for 2-norm.
	\newcommand*{\dx}[1][x]{\mathop{}\!\mathrm{d}#1}
	\newcommand*{\ddx}[1][x]{\mathop{}\!\frac{\mathrm{d}}{\mathrm{d}#1}}
	\newcommand*{\deldx}[1][x]{\mathop{}\!\frac{\del}{\del #1}}
%	\newcommand{\e}{\mathop{}\!\mathrm{e}}
	\newcommand{\bsq}[1][global]{%
		\ensuremath{\color{#1}\blacksquare}%
	}
	\newcommand{\bcont}[1][global]{%
		\text{\color{#1}\Lightning}%
	}
	\newcommand{\with}[1][|]{%
		\ensuremath{\,#1\,}
	}
	
	%% Scaled Symbols
	\newlength{\ndepth}
	\NewDocumentCommand{\nsymbol}{O{1.1} m}{
		\setlength{\ndepth}{\depthof{\ensuremath{#2}}}
		\mathop{%
	        \raisebox{-#1\ndepth+1\ndepth}{
	            	\scalebox{#1}{$\displaystyle#2$}%
	        }
	    }
	}
	\newcommand{\ncup}[1][1.0]{\nsymbol[#1]{\bigcup}}
	\newcommand{\ncap}[1][1.0]{\nsymbol[#1]{\bigcap}}
	\newcommand{\nint}[1][1.1]{\nsymbol[#1]{\int}}
	\newcommand{\nprod}[1][1.0]{\nsymbol[#1]{\prod}}
	\newcommand{\nsum}[1][1.0]{\nsymbol[#1]{\sum}}
	\newcommand{\nland}[1][1.0]{\nsymbol[#1]{\bigwedge}}
	\newcommand{\nlor}[1][1.0]{\nsymbol[#1]{\bigvee}}
	
	
	% Matrices
	\newcommand{\ilvec}[1]{% inline column vector (matrix style)
	  \left(#1\right)^\T%
	}
	
	\newcommand{\icol}[2][c]{% inline column vector (matrix style)
	  \left(\begin{matrix}[#1]#2\end{matrix}\right)%
	}
	
	\newcommand{\irow}[2][c]{% inline row vector (matrix style)
	  \begin{matrix}[#1](#2)\end{matrix}%
	}
	
	\newcommand{\imat}[2][c]{% inline matrix
	  \renewcommand{\arraystretch}{1.1}
	  \left[\begin{matrix}[#1]#2\end{matrix}\right]%
	}
	
	\NewDocumentCommand{\iarr}{O{c} m}{% inline matrix
	  \left[\begin{array}{#1}#2\end{array}\right]%
	}
	
	\newcommand{\divmat}[2][c|c]{% inline matrix with divider line
	  \left[\begin{array}{#1}#2\end{array}\right]%
	}
	
	% For matrix centering
	\renewcommand*\env@matrix[1][c]{\hskip -\arraycolsep
	  \let\@ifnextchar\new@ifnextchar
	  \array{*\c@MaxMatrixCols #1}}
	
	
	%% Replacements:
	
	%% Switch between epsilons
	%\let\oldvarepsilon\varepsilon
	%\let\varepsilon\epsilon
	%\let\epsilon\oldvarepsilon
	
	%% Switch lightning symbol
	\let\marvosymLightning\Lightning
	\let\wasysymLightning\lightning
	
	
	% Replace asterisk with \cdot
	\mathcode`\*="8000
	{\catcode`\*\active\gdef*{\cdot}}
	
	%% Put limits always below operator
	%\apptocmd{\lim}{\limits}{}{}	
	%\apptocmd{\min}{\limits}{}{}	
	%\apptocmd{\max}{\limits}{}{}	
	
	%% Typeset := better.
	%\mathcode`\:=\string"8000
	%\begingroup \catcode`\:=\active
	%  \gdef:{\mathrel{\mathop{\narrowcolon}}}
	%\endgroup
	
	\newcommand*{\defeq}{%
		\mathrel{\vcenter{\baselineskip0.5ex \lineskiplimit0pt
			\hbox{\scriptsize.}\hbox{\scriptsize.}}}=}
	
	% Make subscript smaller (indices)
	%\begingroup\lccode`~=`_
	%\lowercase{\endgroup\def~}#1{_{\scriptscriptstyle#1}}
	%\AtBeginDocument{\mathcode`_="8000 \catcode`_=12 }
	
	% display breaks for align environments.
	\allowdisplaybreaks
	
\fi


\if@loadgraphics 
	\RequirePackage{graphicx, float, caption}
	%
	\DeclareCaptionFont{captionfont}{\sffamily}
	\captionsetup{font=captionfont, labelfont=bf, position=below}
	%\captionsetup[subfloat]{position=top, labelformat=empty}	% TODO: figure out optimal parameters, then include.
\fi


\if@loaddrawing
	\RequirePackage{graphicx, float, tikz, pgfplots}
	%% TODO: insert pre-made commands and styles to select from.
\fi


\if@loadtables
	\RequirePackage{graphicx, multirow, makecell, tabularx}
	
	% Table column/row styles.
	\newcolumntype{a}{>{\columncolor{white}}c}		
	\newcolumntype{+}{>{\global\let\currentrowstyle\relax}}
	\newcolumntype{^}{>{\currentrowstyle}}
	\newcolumntype{L}{>{$}l<{$}}
	\newcolumntype{R}{>{$}r<{$}}
	\newcolumntype{C}{>{$}c<{$}} 
	\newcolumntype{?}[1]{!{\vrule width #1}}
	\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}#1\ignorespaces}

	% Matrix and table spacing
	\renewcommand*{\arraystretch}{1.0}
	\arrayrulecolor{black!40}
\fi

%\if@loadcode
%	\RequirePackage{tcolorbox, tikz, verbatim, fontspec}
%	
%%	\setmonofont[Path={fonts/MonacoB/}, Ligatures=TeX, Scale=0.8,
%%		UprightFont = {*-Regular},
%%		ItalicFont 	= {*-Italic},
%%		BoldFont 	= {*-Bold},
%%		FontFace 	= {m}{it}{*-Italic},
%%		FontFace 	= {mb}{n}{*-Bold},
%%		FontFace 	= {sb}{n}{*-SemiBold},	
%%		FontFace 	= {xb}{n}{*-ExtraBold},
%%	]{MonacoB}
%	
%	\NewDocumentCommand{\codeB}{m O{black!70} O{\currentfontsize}}{\text{%
%	#3%									% depends on scale of font import.
%	\ttfamily\color{#2}#1}}
%
%	\NewDocumentCommand{\codeC}{m O{black!70} O{\scriptsize}}{%
%		\codeB{#1}[#2][#3]
%	}
%	
%	\newcommand*{\codeD}[2][black]{%
%		\tikz[
%			baseline=(n.base),
%			terminal/.style={
%				text=#1, font=\small\ttfamily,
%				rectangle, rounded corners=1, line width=0pt,
%				minimum height={1.0*max(height("#2"), 2ex)}, 
%				preaction={fill=black!50, fill opacity=0.12}, 
%				inner xsep=2pt, inner ysep=1pt,
%			}
%		]{\node [terminal] (n) {\codeB{#2}[black]};}%
%	}
%	
%	\NewDocumentCommand{\code}{O{bg} m O{black!80} O{}}{
%		\ifthenelse{\equal{#1}{bg}}
%		{% bg
%		\tcbox[
%			nobeforeafter, 
%			box align=base, 
%			size=tight, boxsep=0.3ex, left=0.1ex, right=0.1ex,
%			arc=1pt, outer arc=0.5pt,
%			colback=black!8,
%			colframe=white,
%			fontupper=\ttfamily\currentfontsize\relsize{-0.4},
%			coltext=#3,
%			#4
%		]{#2}}
%		{% no bg
%		\text{\ttfamily\currentfontsize\relsize{-0.4}#2}}
%	}
%
%	% TODO: extend with syntax-highlighting environments (minted).
%\fi



%% LAYOUT
% Page style
\RequirePackage{geometry}
\newgeometry{
	ignoreall,
	% body/edge separation:
	top=1.8cm,
	bottom=1.8cm, 
	left=2cm, 
	right=2cm, 
	%
	headheight=1.8cm, 	% height of header.
	headsep=0cm,		% distance btw. header end & body start.
	footskip=1cm,		% distance bwt. body end & footer end.
	voffset=.25cm, 		% distance btw. edge & header start.
}
\setlength{\parindent}{0pt}
\setlength{\marginparsep}{0cm}
\setlength{\marginparwidth}{1.5cm}

% Header/Footer style:
\RequirePackage{fancyhdr}
\pagestyle{fancy} % use "empty" if page numbers not wanted
\fancyhf{}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.2pt}
\patchcmd{\headrule}{\hrule}{\color{gray!25}\hrule}{}{}
\patchcmd{\footrule}{\hrule}{\color{gray!25}\hrule}{}{}
%\fancyhead[L]{}
\if@nobanner
	\fancyhead[L]{}
\else
	\fancyhead[L]{
		\makeatletter
		%\vskip-0.35cm
		 \begin{tcolorbox}[
		 	width=1.0\linewidth + \Gm@lmargin + \Gm@rmargin + 0.2cm,
		 	enlarge left by=-\dimexpr(\Gm@lmargin + 0.1cm), 
		 	enlarge right by=-\dimexpr(\Gm@rmargin + 0.1cm), 
		 	colback=global_banner_bg,
		 	colframe=global_banner_bg!30,
		 	boxrule=0.5pt,
		 	height=0.9cm,
		 	top=0.4mm,
		 	left=0.55cm, right=0.55cm,
		 	arc=0pt,
		 	]
				\parbox[l]{0.5\linewidth}{%
					\color{global_banner_text}\tiny \HWtext@course\HWtext@subtitledividerchar\HWtext@semesterPart
					\mbox{}\\[-2pt]%
					\mbox{}
					\hskip-0.5ex
					\large\textbf{\@title
						\ifnum\theseries>0 
							\theseries
						\fi }
						}%
				\hfill\parbox[r]{0.5\linewidth}{%
					\color{global_banner_text}%
					\hfill \tiny \DTMtoday \hskip-1pt \\[2pt]%
					%\mbox{} \hfill \tiny due : \quad \DTMusedate{dueDate} \\[0pt]%
					\mbox{} \hfill \tiny \HWtext@contributors \HWtext@groupdividerchar \HWtext@group%
				}%
		    \end{tcolorbox}%
		%\vskip-\dimexpr(\Gm@tmargin-1cm)
		\makeatother
	}
\fi
\fancyfoot[C]{\textcolor{gray!25}{\thepage}}



