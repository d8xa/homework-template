# LaTeX class for homework submissions.

This project provides a modular LaTeX homework class with tcolorbox environment for problem definitions, many math commands for convenience, and a clean, modern look.


### License

LPPL 1.3.c.

### How to use

1. Download or clone the project. 
2. Put the class file in your working directory, or install locally in your TEXMF folder.